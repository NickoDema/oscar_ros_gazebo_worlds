from glob import glob
from setuptools import setup

PACKAGE_NAME = 'alpha_gazebo_worlds'

setup(
    name=PACKAGE_NAME,
    version='0.1.0',
    packages=[PACKAGE_NAME],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + PACKAGE_NAME]),
        ('share/' + PACKAGE_NAME, ['package.xml']),
        ('share/' + PACKAGE_NAME + '/launch', glob('launch/*.launch.py'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Nikolay Dema',
    maintainer_email='ndema2301@gmail.com',
    description='Package with a launch file to start a simulated Gazebo world',
    license='BSD, Apache 2.0',
    entry_points={
        'console_scripts': [
        ],
    },
)
